jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        if ($.exists(".navbar-toggle")) {
            $('.navbar-toggle').on('click', function(){
               $('.navbar-collapse').toggleClass('active').stop().slideToggle('fast');
            });
        }


        if ($.exists(".checkbox-sw>label")) {
            $('.checkbox-sw>label').on('click',function(e){
                var el = $(e.currentTarget);
                console.log(el.prev('input'));
                if (el.prev('input').is(':checked')){
                    el.parent().addClass('checked');
                } else {
                    el.parent().removeClass('checked');
                }
            });
        }


        if ($.exists(".animBtn")) {
            $('.animBtn').on('click',function(e){
                var animation = function(){
                    e.preventDefault();
                    var el = $(e.currentTarget);
                    $('.animBox').addClass('active');
                    $(el).parents('.animBox').css({zIndex: 10});

                    $('.animBox1').animate({right: '-360px'},500);
                    $('.animBox2').animate({right: '-120px'},500);
                    $('.animBox3').animate({right: '120px'},500);
                    $('.animBox4').animate({right: '360px'},500);

                    var redirect = function(){
                        window.location.href = "register-step2.html";
                    };
                    setTimeout(redirect, 500);
                };

                var animation2 = function(){
                    e.preventDefault();
                    var el = $(e.currentTarget);
                    $('.animBox').addClass('active');
                    $(el).parents('.animBox').css({zIndex: 10});

                    $('.animBox1').animate({right: '-435px'},500);
                    $('.animBox2').animate({right: '-145px'},500);
                    $('.animBox3').animate({right: '145px'},500);
                    $('.animBox4').animate({right: '435px'},500);

                    var redirect = function(){
                        window.location.href = "register-step2.html";
                    };
                    setTimeout(redirect, 500);
                };


                if($( window ).width() > 991 && $( window ).width() < 1199){
                    animation();
                }
                $( window ).resize(function() {
                    if($( window ).width() > 991 && $( window ).width() < 1199){
                        animation();
                    }
                });

                if($( window ).width() > 1199){
                    animation2();
                }
                $( window ).resize(function() {
                    if($( window ).width() > 1199){
                        animation2();
                    }
                });
            });
        }


        if($.exists('.formCheck')){
            $('.formCheck').on('submit', function (event) {
                var form = $(this);
                var inputCheck = form.find('.inputCheck');
                if (inputCheck.val().length < 2) {
                    $(inputCheck).addClass('check-error');
                    event.preventDefault();
                }
            });
            $('.inputCheck').focus(function() {
                $(this).removeClass('check-error');
            });
        }


        if ($.exists("#inputFile")) {
            $("#inputFile").fileinput({
                uploadUrl: "/file-upload-batch/2"
            });
        }


        if ($.exists(".openHidden")) {
            $('.openHidden').on('click', function(){
                $('.hiddenBox').not(this).stop().slideUp('fast');
                $(this).children('.hiddenBox').toggleClass('active').stop().slideToggle('fast');
            });
        }


        if ($.exists(".companySlider")) {
            $('.companySlider').lightSlider({
                adaptiveHeight:true,
                item:1,
                slideMargin:0,
                loop:true
            });
        }


        if ($.exists(".hasTooltip")) {
            $('.hasTooltip').hover(function(){
                $(this).next().stop().slideToggle('fast');
            });
        }


        if ($.exists(".inputSar")) {
            $(".inputSar").rating();
        }


        if ($.exists(".openPopup")) {
            $('.openPopup').on('click', function(){
                $('.cd-popup').toggleClass('is-visible');
            });
        }


        if ($.exists(".chosen-select")) {
            $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
        }


        //if ($.exists(".dropdown")) {
        //    $(".dropdown").on("click", ".add-tag", function() {
        //
        //        if ( $(".dropdown").hasClass("open") ) {
        //            $(".dropdown").removeClass("open");
        //        } else {
        //            $(".dropdown").addClass("open");
        //        }
        //    });
        //
        //    // Add Tags
        //    $(".dropdown").on("click", ".dropdown-menu > li", function() {
        //        if ( !$(this).hasClass("added") ) {
        //            $(this).addClass("added");
        //
        //            $(".tag-area").append('<div class="tag">' + $(this).text() + '<span>?</span></div>');
        //        }
        //    });
        //
        //    // Remove Tags
        //    $(".wrapper").on("click", ".tag > span", function() {
        //        $(this).parent().remove();
        //
        //        var objectText = $(this).parent().text().slice(0,-1);
        //
        //        $(".dropdown-menu > li:contains('" + objectText + "')").removeClass("added");
        //    });
        //}


        if ($.exists(".sponsoredResult, .advertisingBox")) {
            var appEndResult = function(){
                if($( window ).width() < 481){
                    $('.sponsoredResult').each(function(){
                        $(this).appendTo($(this).parent());
                    });
                }else{
                    $('.sponsoredResult').each(function(){
                        $(this).prependTo($(this).parent());
                    });
                }
            };

            appEndResult();
            $( window ).resize(function() {
                appEndResult();
            });
        }


        if ($.exists(".js-single")) {
            $(".js-single").select2();
        }


        if ($.exists(".serviceForm")) {
            $('.serviceForm').on('submit', function(e){
                if(!($(this).find('.serviceRadio input').attr('checked'))){
                    e.preventDefault();
                    console.log('vax');
                }
            });
        }


        if ($.exists(".accordion-toggle")) {
            $('.accordion-toggle').on('click', function(){
                $(this).toggleClass('active');

                if($(this).hasClass('active')){
                    $(this).find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                }else{
                    $(this).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                }


                $('.accordion-toggle').not(this).each(function(){
                    $(this).removeClass('active');
                    $(this).find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                });
            });
        }


        if ($.exists(".trackingCopy")) {
            $(".trackingCopy").zclip({
                path: "http://www.steamdev.com/zclip/js/ZeroClipboard.swf",
                copy: function(){
                    return $(this).parent().children('h3').text();
                }
            });
        }


        if ($.exists("#tabledata")) {
            $("#tabledata").searcher({
                inputSelector: "#tablesearchinput"
            });
            $('#tabledata').DataTable();
        }


        if ($.exists(".calcCommission")) {
            var sum = 0;
            $('.calcCommission tr td:last-child').not('td.color1').each(function() {
                sum += Number($(this).text());
            });
            $('.totalCalc').html(sum);
        }


        if ($.exists(".dataRange")) {
            $('.dataRange').dateRangePicker({
                autoClose: false,
                format: 'YYYY-MM-DD',
                separator: ' to ',
                language: 'auto',
                startOfWeek: 'sunday',// or monday
                getValue: function()
                {
                    return $(this).val();
                },
                setValue: function(s)
                {
                    if(!$(this).attr('readonly') && !$(this).is(':disabled') && s != $(this).val())
                    {
                        $(this).val(s);
                    }
                },
                startDate: false,
                endDate: false,
                time: {
                    enabled: false
                },
                minDays: 0,
                maxDays: 0,
                showShortcuts: false,
                shortcuts:
                {
                    //'prev-days': [1,3,5,7],
                    //'next-days': [3,5,7],
                    //'prev' : ['week','month','year'],
                    //'next' : ['week','month','year']
                },
                customShortcuts : [],
                inline:false,
                container:'body',
                alwaysOpen:false,
                singleDate:false,
                lookBehind: false,
                batchMode: false,
                duration: 200,
                stickyMonths: false,
                dayDivAttrs: [],
                dayTdAttrs: [],
                applyBtnClass: '',
                singleMonth: 'auto',
                hoveringTooltip: function(days, startTime, hoveringTime)
                {
                    return days > 1 ? days + ' ' + lang('days') : '';
                },
                showTopbar: true,
                swapTime: false,
                selectForward: false,
                selectBackward: false,
                showWeekNumbers: false,
                getWeekNumber: function(date) //date will be the first day of a week
                {
                    return moment(date).format('w');
                }
            });
        }

        if ($.exists("")) {

        }

        if ($.exists("")) {

        }

        if ($.exists("")) {

        }



    });
}).call(this);




